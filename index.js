#!/usr/bin/env node
"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const badge_up_1 = __importDefault(require("badge-up"));
const latest_version_1 = __importDefault(require("latest-version"));
const express_1 = __importDefault(require("express"));
const join_path_1 = __importDefault(require("join-path"));
const util_1 = __importDefault(require("util"));
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
const badge = util_1.default.promisify(badge_up_1.default);
const appendFile = util_1.default.promisify(fs_1.default.appendFile);
const config = require(path_1.default.resolve('config.json')) || {};
const app = express_1.default();
const getBadge = (req, res, next) => {
    const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    const log = `${new Date()} ${ip} ${req.headers['referer']} ${req.headers['user-agent']} - ${req.params.user}:${req.params.repo}:${req.params.badge}`;
    if (config.log) {
        appendFile(config.log, log + '\n').catch((error) => {
            console.error(error);
        });
    }
    else {
        console.log(log);
    }
    const badgeConfig = config.badges && config.badges.find((badge) => {
        if (badge.user !== req.params.user
            || (badge.repos && badge.repos.indexOf(req.params.repo) === -1)
            || (badge.badges && badge.badges.indexOf(req.params.badge) === -1)) {
            return;
        }
        return true;
    });
    if (!badgeConfig) {
        res.status(404).end();
        return;
    }
    switch (req.params.badge) {
        case 'npm':
            latest_version_1.default(req.params.repo).then((version) => {
                return badge('npm', `v${version}`, badge_up_1.default.colors.blue);
            }).then((badge) => {
                res.type('svg').send(badge).end();
            }, (error) => {
                return badge('npm', `unreleased`, badge_up_1.default.colors.blue).then((badge) => {
                    res.type('svg').send(badge).end();
                });
            });
            break;
        case 'custom':
            badge(req.query.name && req.query.name.replace('_', ' '), req.query.value && req.query.value.replace('_', ' '), (req.query.color && badge_up_1.default.colors[req.query.color]) || badge_up_1.default.colors.lightgrey)
                .then((badge) => {
                res.type('svg').send(badge).end();
            }, (error) => {
                console.error(error);
                res.status(404).end();
            });
            break;
        default:
            res.status(404).end();
    }
};
app.get(join_path_1.default(config.baseUri || '/', ':user/:repo/:badge'), getBadge);
const port = config.port || 5570;
const hostname = config.hostname || 'localhost';
app.listen(port, hostname, () => {
    console.log(`badge-server ready and listening on ${hostname}:${port}`);
});
