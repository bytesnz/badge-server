badge-server
=================
Simple express server for serving badges

[![npm](https://bytes.nz/b/badge-server/npm)](https://gitlab.com/bytesnz/badge-server)

Once running, badges can be generated and retrieved with the URI
`/:user/:package/:badge`, eg `/bytesnz/badge-server/npm`.

## Available Badges
- `npm` - [![npm](https://bytes.nz/b/badge-server/npm)](https://gitlab.com/bytesnz/badge-server)
- `custom?name=hello&color=lightgrey&value=world` - ![hello](https://bytes.nz/b/badge-server/custom?name=hello&value=world)

## Configuration
The available badges need to be configured in a `config.json` file that should
be located in the current working directory.


```typescript
/**
 * Definition for the configuration file for badge-server
 * Should be located in the current working directory
 */
export interface Config {
  /// Base URI that badges will be served from. Defaults to /
  baseUri?: string,
  /// Port to listen on. Defaults to 5570
  port?: number,
  /// Hostname to listen on. Default to localhost
  hostname?: string
  /// Log file to write requests to. Default to logging to console
  log?: string,
  /// Badges to serve
  badges: Array<{
    /// Username that owns the repos/packages
    user?: string,
    /// Repo(s)/Package(s) to server badges for. If not given, badges for any
    /// repo/package will be servered. *WARNING* this will mean any package
    /// for NPM badges (as the user is not used by this badge
    repos?: Array<string>,
    /// Badge(s) to serve
    badges?: Array<string>
  }>
}


```


# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.0] - 2018-07-10
Initial Release
