badge-server
=================
Simple express server for serving badges

[![npm](https://bytes.nz/b/badge-server/npm)](https://gitlab.com/bytesnz/badge-server)

Once running, badges can be generated and retrieved with the URI
`/:user/:package/:badge`, eg `/bytesnz/badge-server/npm`.

## Available Badges
- `npm` - [![npm](https://bytes.nz/b/badge-server/npm)](https://gitlab.com/bytesnz/badge-server)
- `custom?name=hello&color=lightgrey&value=world` - ![hello](https://bytes.nz/b/badge-server/custom?name=hello&value=world)

## Configuration
The available badges need to be configured in a `config.json` file that should
be located in the current working directory.
