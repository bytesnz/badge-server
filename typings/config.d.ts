/**
 * Definition for the configuration file for badge-server
 * Should be located in the current working directory
 */
export interface Config {
  /// Base URI that badges will be served from. Defaults to /
  baseUri?: string,
  /// Port to listen on. Defaults to 5570
  port?: number,
  /// Hostname to listen on. Default to localhost
  hostname?: string
  /// Log file to write requests to. Default to logging to console
  log?: string,
  /// Badges to serve
  badges: Array<{
    /// Username that owns the repos/packages
    user?: string,
    /// Repo(s)/Package(s) to server badges for. If not given, badges for any
    /// repo/package will be servered. *WARNING* this will mean any package
    /// for NPM badges (as the user is not used by this badge
    repos?: Array<string>,
    /// Badge(s) to serve
    badges?: Array<string>
  }>
}

