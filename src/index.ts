#!/usr/bin/env node

import badgeUp from 'badge-up';
import latestVersion from 'latest-version';
import express from 'express';
import joinPath from 'join-path';
import util from 'util';
import fs from 'fs';
import path from 'path';

import { Config } from '../typings/config';

const badge = util.promisify(badgeUp);
const appendFile = util.promisify(fs.appendFile);

const config: Config = require(path.resolve('config.json')) || {};

const app = express();

const getBadge = (req, res, next) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const log = `${new Date()} ${ip} ${req.headers['referer']} ${req.headers['user-agent']} - ${req.params.user}:${req.params.repo}:${req.params.badge}`;
  if (config.log) {
    appendFile(config.log, log + '\n').catch((error) => {
      console.error(error);
    });
  } else {
    console.log(log);
  }

  const badgeConfig = config.badges && config.badges.find((badge) => {
    if (badge.user !== req.params.user
        || (badge.repos && badge.repos.indexOf(req.params.repo) === -1)
        || (badge.badges && badge.badges.indexOf(req.params.badge) === -1)) {
      return;
    }

    return true;
  });

  if (!badgeConfig) {
    res.status(404).end();
    return;
  }

  switch (req.params.badge) {
    case 'npm':
      latestVersion(req.params.repo).then((version) => {
        return badge('npm', `v${version}`, badgeUp.colors.blue);
      }).then((badge) => {
        res.type('svg').send(badge).end();
      }, (error) => {
        return badge('npm', `unreleased`, badgeUp.colors.blue).then((badge) => {
          res.type('svg').send(badge).end();
        });
      });
      break;
    case 'custom':
      badge(req.query.name && req.query.name.replace('_', ' '),
          req.query.value && req.query.value.replace('_', ' '),
          (req.query.color && badgeUp.colors[req.query.color]) || badgeUp.colors.lightgrey)
          .then((badge) => {
        res.type('svg').send(badge).end();
      }, (error) => {
        console.error(error);
        res.status(404).end();
      });
      break;
    default:
      res.status(404).end();
  }
};

app.get(joinPath(config.baseUri || '/', ':user/:repo/:badge'), getBadge);

const port = config.port || 5570;
const hostname = config.hostname || 'localhost'
app.listen(port, hostname, () => {
  console.log(`badge-server ready and listening on ${hostname}:${port}`);
});
