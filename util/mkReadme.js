#!/usr/bin/env node

const fs = require('fs');

fs.writeFileSync('README.md', `${fs.readFileSync('README.src.md')}

\`\`\`typescript
${fs.readFileSync('typings/config.d.ts')}
\`\`\`


${fs.readFileSync('CHANGELOG.md')}`);
